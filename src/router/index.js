import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      permission: 'public',
    },
  },
  {
    path: '/acl',
    redirect: '/acl/home',
    name: 'ACL',
    meta: {
      permission: 'logged',
    },
    component: {
      render(c) {
        return c('router-view')
      }
    },
    children: [
      {
        path: 'home',
        name: 'ACLHome',
        meta: {
          permission: 'logged',
        },
        component: () => import('../views/Acl.vue')
      }
    ]
  },
  {
    path: '/403',
    name: 'Page403',
    meta: {
      permission: 'public'
    },
    component: () => import('../views/403.vue')
  },
  {
    path: '*',
    name: 'Default404',
    meta: {
      permission: 'public'
    },
    component: () => import('../views/404.vue')
  },
]

const router = new VueRouter({
  routes
})

export default router
