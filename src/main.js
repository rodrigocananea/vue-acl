import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Acl from './plugins/vuex-acl'

import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'

Vue.use(Acl, {
  router,
  init: "public",
  store,
  fail: "/403"
})

Vue.use(Antd)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
