import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  namespaced: true,
  state: {
    acl_current: ['public']
  },
  mutations: {
    LOAD_RULES(state, rules) {
      state.acl_current = rules
    }
  },
})
